package com.wipro.tutorial.at.steps;

import com.wipro.tutorial.at.pages.LoginPage;
import org.jbehave.core.annotations.Given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginSteps extends AbstractSteps {

	@Autowired
	private LoginPage loginPage;
	
	@Given("User is on Login Page")
	public void userGoesLoginPage() {		
		loginPage.navigateTo();
	}
}
