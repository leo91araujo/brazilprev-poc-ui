package com.wipro.tutorial.at.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.springframework.beans.factory.annotation.Value;

import com.wipro.tutorial.at.configuration.pageobjects.PageObject;

@PageObject
public class LoginPage extends AbstractPage {
		
	@Value("${app.url}")
	private String SAMPLE_BANK_URL;
	
	@FindBy(how = How.ID, using = "lst-ib")
	private WebElement searchInput;

	@FindBy(how = How.NAME, using = "btnK")
	private WebElement searchButton;

	public void navigateTo() {
		LOG.info("Navigating user to page: " + SAMPLE_BANK_URL);
		webDriverProvider.get().get(SAMPLE_BANK_URL);

		search("BrasilPrev");
	}

	public void search (String criteria) {
		searchInput.clear();
		searchInput.sendKeys(criteria);
		searchButton.click();
	}
}
