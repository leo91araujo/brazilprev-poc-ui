package com.wipro.tutorial.at.configuration.provider;

import org.jbehave.web.selenium.PropertyWebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class IEPropertyWebDriverProvider extends PropertyWebDriverProvider {
    public IEPropertyWebDriverProvider() {
    }

    public void initialize() {
        org.jbehave.web.selenium.PropertyWebDriverProvider.Browser browser = (org.jbehave.web.selenium.PropertyWebDriverProvider.Browser) org.jbehave.web.selenium.PropertyWebDriverProvider.Browser.valueOf(org.jbehave.web.selenium.PropertyWebDriverProvider.Browser.class, System.getProperty("browser", "firefox").toUpperCase(this.usingLocale()));
        this.delegate.set(createDriver());
    }

    private WebDriver createDriver() {
        DesiredCapabilities IEcaps = DesiredCapabilities.internetExplorer();
        IEcaps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        return new InternetExplorerDriver(IEcaps);
    }
}

